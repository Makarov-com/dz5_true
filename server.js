const express = require('express');
const es6Renderer = require("express-es6-template-engine");
const MainController = require('./controllers/main-controller');
const config = require('./config.json')
const MongoService = require('./services/mongo.service');
const UserController = require('./controllers/user-controller.js');
const cors = require("cors");


const app = express();
app.engine("html", es6Renderer);
app.set("views", __dirname + "/views");
app.use(express.static("public"));
app.use(express.json());
app.use(cors());

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

(async () => {
    await new MongoService(config.mongo).init();

    new MainController(app);
    new UserController(app);
    

    app.listen(3000, () => {
        console.log("Server is up!");
    })
})();

app.get("/", (req, res) => {
    res.render("index.html", { locals: { title: "DZ5" } });
});

